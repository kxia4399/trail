<p style="color: red; font-weight: bold">>>>>>  gd2md-html alert:  ERRORs: 0; WARNINGs: 0; ALERTS: 16.</p>
<ul style="color: red; font-weight: bold"><li>See top comment block for details on ERRORs and WARNINGs. <li>In the converted Markdown or HTML, search for inline alerts that start with >>>>>  gd2md-html alert:  for specific instances that need correction.</ul>

<p style="color: red; font-weight: bold">Links to alert messages:</p><a href="#gdcalert1">alert1</a>
<a href="#gdcalert2">alert2</a>
<a href="#gdcalert3">alert3</a>
<a href="#gdcalert4">alert4</a>
<a href="#gdcalert5">alert5</a>
<a href="#gdcalert6">alert6</a>
<a href="#gdcalert7">alert7</a>
<a href="#gdcalert8">alert8</a>
<a href="#gdcalert9">alert9</a>
<a href="#gdcalert10">alert10</a>
<a href="#gdcalert11">alert11</a>
<a href="#gdcalert12">alert12</a>
<a href="#gdcalert13">alert13</a>
<a href="#gdcalert14">alert14</a>
<a href="#gdcalert15">alert15</a>
<a href="#gdcalert16">alert16</a>

<p style="color: red; font-weight: bold">>>>>> PLEASE check and correct alert issues and delete this message and the inline alerts.<hr></p>



[TOC]



## **Statement of work done：**

As we are adopting the method of XP in our project, we self-organize around the problem to solve it with assigned roles and work divided correspondingly.

Since we establish the weekly meeting with our clients,i have been assigned with the part how to enable reviewers to interact with front and thus re-highlight the pdf file with different evidence mainly since week_3. Additionally, i also put the effort in the very first task out group can’t avoid,which is try to convert pdf file into html. Finally, there are some task assigned regarding to the assignment and weekly report for unit 3888, where my part includes tutor minutes from the W2-W6, presentation about the team group process quality and the introduction part in the group project. 

So simply i will illustrate the work done below with evidence attached:

**Finish the tutor minutes from w2-w6**

Evidence from the bitbucket screenshot:

<p id="gdcalert1" ><span style="color: red; font-weight: bold">>>>>>  gd2md-html alert: inline image link here (to images/3888-individual0.png). Store image on your image server and adjust path/filename if necessary. </span><br>(<a href="#">Back to top</a>)(<a href="#gdcalert2">Next alert</a>)<br><span style="color: red; font-weight: bold">>>>>> </span></p>


![alt_text](images/3888-individual0.png "image_tooltip")


	

<p id="gdcalert2" ><span style="color: red; font-weight: bold">>>>>>  gd2md-html alert: inline image link here (to images/3888-individual1.png). Store image on your image server and adjust path/filename if necessary. </span><br>(<a href="#">Back to top</a>)(<a href="#gdcalert3">Next alert</a>)<br><span style="color: red; font-weight: bold">>>>>> </span></p>


![alt_text](images/3888-individual1.png "image_tooltip")




<p id="gdcalert3" ><span style="color: red; font-weight: bold">>>>>>  gd2md-html alert: inline image link here (to images/3888-individual2.png). Store image on your image server and adjust path/filename if necessary. </span><br>(<a href="#">Back to top</a>)(<a href="#gdcalert4">Next alert</a>)<br><span style="color: red; font-weight: bold">>>>>> </span></p>


![alt_text](images/3888-individual2.png "image_tooltip")



        find an alternative way to transfer html into pdf: 

	

<p id="gdcalert4" ><span style="color: red; font-weight: bold">>>>>>  gd2md-html alert: inline image link here (to images/3888-individual3.png). Store image on your image server and adjust path/filename if necessary. </span><br>(<a href="#">Back to top</a>)(<a href="#gdcalert5">Next alert</a>)<br><span style="color: red; font-weight: bold">>>>>> </span></p>


![alt_text](images/3888-individual3.png "image_tooltip")


	     3.Presentation on the group process quality including making slides: 

	

<p id="gdcalert5" ><span style="color: red; font-weight: bold">>>>>>  gd2md-html alert: inline image link here (to images/3888-individual4.png). Store image on your image server and adjust path/filename if necessary. </span><br>(<a href="#">Back to top</a>)(<a href="#gdcalert6">Next alert</a>)<br><span style="color: red; font-weight: bold">>>>>> </span></p>


![alt_text](images/3888-individual4.png "image_tooltip")


	   4. modify the slides in terms of back-end stuff



<p id="gdcalert6" ><span style="color: red; font-weight: bold">>>>>>  gd2md-html alert: inline image link here (to images/3888-individual5.png). Store image on your image server and adjust path/filename if necessary. </span><br>(<a href="#">Back to top</a>)(<a href="#gdcalert7">Next alert</a>)<br><span style="color: red; font-weight: bold">>>>>> </span></p>


![alt_text](images/3888-individual5.png "image_tooltip")


	

5. partially finish the most important part- enable user to re-highlight and stored in database(this is basically, update the csv file for each pdf file)


## **Extent of that work**

I will mainly talk about two technical work that i have put the effort on.It is listed below each with a demo.


### **Re-highlight on the pdf** 

I made a demo html and i will show below and also this part is mentioned during the presentation.

User story: 

1.Suppose this is the pdf file loaded from the first reviewer for the 2nd reviewer



<p id="gdcalert7" ><span style="color: red; font-weight: bold">>>>>>  gd2md-html alert: inline image link here (to images/3888-individual6.png). Store image on your image server and adjust path/filename if necessary. </span><br>(<a href="#">Back to top</a>)(<a href="#gdcalert8">Next alert</a>)<br><span style="color: red; font-weight: bold">>>>>> </span></p>


![alt_text](images/3888-individual6.png "image_tooltip")


2. Oh no! The previous reviewed made a mistake in the evidence for the 4th creteria (highlighted in blue color),i should change it. And the 7th evidence is aslo wrong, let me just erase that.



<p id="gdcalert8" ><span style="color: red; font-weight: bold">>>>>>  gd2md-html alert: inline image link here (to images/3888-individual7.png). Store image on your image server and adjust path/filename if necessary. </span><br>(<a href="#">Back to top</a>)(<a href="#gdcalert9">Next alert</a>)<br><span style="color: red; font-weight: bold">>>>>> </span></p>


![alt_text](images/3888-individual7.png "image_tooltip")


		

	

3. Oh no! the 1st reviewer might actually be right , i should recover to its version, i simply click deseralize.	



<p id="gdcalert9" ><span style="color: red; font-weight: bold">>>>>>  gd2md-html alert: inline image link here (to images/3888-individual8.png). Store image on your image server and adjust path/filename if necessary. </span><br>(<a href="#">Back to top</a>)(<a href="#gdcalert10">Next alert</a>)<br><span style="color: red; font-weight: bold">>>>>> </span></p>


![alt_text](images/3888-individual8.png "image_tooltip")


 

link below: [https://bitbucket.org/ccai6142/soft3888_m16a_group6/commits/c62ee1d4fe6f14ec6e312d12d58c0872b0d3c6b0](https://bitbucket.org/ccai6142/soft3888_m16a_group6/commits/c62ee1d4fe6f14ec6e312d12d58c0872b0d3c6b0)



<p id="gdcalert10" ><span style="color: red; font-weight: bold">>>>>>  gd2md-html alert: inline image link here (to images/3888-individual9.png). Store image on your image server and adjust path/filename if necessary. </span><br>(<a href="#">Back to top</a>)(<a href="#gdcalert11">Next alert</a>)<br><span style="color: red; font-weight: bold">>>>>> </span></p>


![alt_text](images/3888-individual9.png "image_tooltip")




<p id="gdcalert11" ><span style="color: red; font-weight: bold">>>>>>  gd2md-html alert: inline image link here (to images/3888-individual10.png). Store image on your image server and adjust path/filename if necessary. </span><br>(<a href="#">Back to top</a>)(<a href="#gdcalert12">Next alert</a>)<br><span style="color: red; font-weight: bold">>>>>> </span></p>


![alt_text](images/3888-individual10.png "image_tooltip")


Referred to the link in the previous section, This part is expected to be the core function to support reviewers to re-highlight the pdf,which is confirmed in our scope with weekly-communication with our clients.Once this is completely finished, combined with what tianyu has supported the automatic highlighting function,the core function will be achieved and we can integrate with the front and database both in normal ways,should be achieved easily.


### 
         **Alternative way to transfer html into pdf**



<p id="gdcalert12" ><span style="color: red; font-weight: bold">>>>>>  gd2md-html alert: inline image link here (to images/3888-individual11.png). Store image on your image server and adjust path/filename if necessary. </span><br>(<a href="#">Back to top</a>)(<a href="#gdcalert13">Next alert</a>)<br><span style="color: red; font-weight: bold">>>>>> </span></p>


![alt_text](images/3888-individual11.png "image_tooltip")


I am running on my local server to test this function.

We enter the web to build the front to start the demo.



<p id="gdcalert13" ><span style="color: red; font-weight: bold">>>>>>  gd2md-html alert: inline image link here (to images/3888-individual12.png). Store image on your image server and adjust path/filename if necessary. </span><br>(<a href="#">Back to top</a>)(<a href="#gdcalert14">Next alert</a>)<br><span style="color: red; font-weight: bold">>>>>> </span></p>


![alt_text](images/3888-individual12.png "image_tooltip")


The 1.pdf is the real-example given by client,which is a medical article.

 After we click 1.pdf, the back automatically convert that to html file,



<p id="gdcalert14" ><span style="color: red; font-weight: bold">>>>>>  gd2md-html alert: inline image link here (to images/3888-individual13.png). Store image on your image server and adjust path/filename if necessary. </span><br>(<a href="#">Back to top</a>)(<a href="#gdcalert15">Next alert</a>)<br><span style="color: red; font-weight: bold">>>>>> </span></p>


![alt_text](images/3888-individual13.png "image_tooltip")




<p id="gdcalert15" ><span style="color: red; font-weight: bold">>>>>>  gd2md-html alert: inline image link here (to images/3888-individual14.png). Store image on your image server and adjust path/filename if necessary. </span><br>(<a href="#">Back to top</a>)(<a href="#gdcalert16">Next alert</a>)<br><span style="color: red; font-weight: bold">>>>>> </span></p>


![alt_text](images/3888-individual14.png "image_tooltip")




<p id="gdcalert16" ><span style="color: red; font-weight: bold">>>>>>  gd2md-html alert: inline image link here (to images/3888-individual15.png). Store image on your image server and adjust path/filename if necessary. </span><br>(<a href="#">Back to top</a>)(<a href="#gdcalert17">Next alert</a>)<br><span style="color: red; font-weight: bold">>>>>> </span></p>


![alt_text](images/3888-individual15.png "image_tooltip")


You can inspect the source and find that within secs, the progress is successfully done.

		 	 	 		

			


## **Quality of technical work done:** 

Generally, not only have i simply try the code to work what is expected to achieve, but also try to figure out whether the reference i use have redundant code and try to understand it, which after i can modify it according to our project scope. Besides,i tend to take even trivial

details into consideration such that there are 10 colors for 10 criterion since it is the format in pedro,which is a medical pdf database. Also regarding to the previous demo on re-highlight function, the serialize function is added since reviewer might mis-highlight some evidence and realize it(wanna modify it then).This is all to the consideration of the real user story when applied.

For the testing part, since there are 10 colors needed and we naturally would like to enable users to distinguish them instead of get messed up with colorful and similar button.So, the  color is adjusted from time to time and finally the difference of color is within acceptable range now.

**Link below for the above**

[https://bitbucket.org/ccai6142/soft3888_m16a_group6/commits/c62ee1d4fe6f14ec6e312d12d58c0872b0d3c6b0](https://bitbucket.org/ccai6142/soft3888_m16a_group6/commits/c62ee1d4fe6f14ec6e312d12d58c0872b0d3c6b0)

	

		 	 	 		

			

				

					

						


## **Other contribution to group processes**：

					

				


## **Writing and wiki use quality: **

			

		

					

				

			

		

			


             	


    	

				

			

		
